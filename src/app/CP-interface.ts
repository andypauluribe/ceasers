
export interface Route {
  Type: string;
  Area: string;
  Source: string[];
  Destination: string[];
  Location: string;
  Room: string;
  Color: string;
}
