import { Component, OnInit } from '@angular/core';
import {Route} from '../CP-interface';
import {MatTableDataSource} from '@angular/material';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public currentRoutes: Route[] = [
    {
      Type: 'AudioInput',
      Area: 'Forum',
      Source: ['F101 - MICLINE 1', 'F101 - MICLINE 2'],
      Destination: ['101 INPUT 1', '101 INPUT 2'],
      Location: 'IDF-A',
      Room: '101 - 102',
      Color: '#FF0000'
    },
    {
      Type: 'AudioInput',
      Area: 'Forum',
      Source: ['F110 - MICLINE 1', 'F104 - MICLINE 6', 'F119 - MICLINE 1'],
      Destination: ['110 INPUT 1', '101 INPUT 9', '114 INPUT 1'],
      Location: 'IDF-H',
      Room: '110',
      Color: '#0000FF'
    },
    {
      Type: 'AudioOutput',
      Area: 'Forum',
      Source: ['F110 - MICLINE 1', 'F104 - MICLINE 6', 'F119 - MICLINE 1'],
      Destination: ['110 INPUT 1', '101 INPUT 9', '114 INPUT 1'],
      Location: 'IDF-H',
      Room: '110',
      Color: '#0000FF'
    },
    {
      Type: 'AudioInput',
      Area: 'Place',
      Source: ['F119 - MICLINE 1'],
      Destination: ['114 INPUT 1'],
      Location: 'IDF-W',
      Room: '114',
      Color: '#00FF00'
    },
    {
      Type: 'AudioInput',
      Area: 'Forum',
      Source: ['F104 - MICLINE 6', 'F119 - MICLINE 1'],
      Destination: ['101 INPUT 9', '114 INPUT 1'],
      Location: 'IDF-C',
      Room: '101 - 102',
      Color: '#FF0000'
    },
    {
      Type: 'AudioInput',
      Area: 'Home',
      Source: ['F110 - MICLINE 1', 'F104 - MICLINE 6', 'F119 - MICLINE 1'],
      Destination: ['110 INPUT 1', '101 INPUT 9', '114 INPUT 1'],
      Location: 'IDF-O',
      Room: '111',
      Color: '#0000FF'
    },
    {
      Type: 'VideoInput',
      Area: 'Place',
      Source: ['F119 - MICLINE 1'],
      Destination: ['114 INPUT 1'],
      Location: 'IDF-W',
      Room: '114',
      Color: '#00FF00'
    },
    {
      Type: 'VideoInput',
      Area: 'Forum',
      Source: ['F104 - MICLINE 6', 'F119 - MICLINE 1'],
      Destination: ['101 INPUT 9', '114 INPUT 1'],
      Location: 'IDF-C',
      Room: '101 - 102',
      Color: '#FF0000'
    },
    {
      Type: 'VideoOutput',
      Area: 'Home',
      Source: ['F110 - MICLINE 1', 'F104 - MICLINE 6', 'F119 - MICLINE 1'],
      Destination: ['110 INPUT 1', '101 INPUT 9', '114 INPUT 1'],
      Location: 'IDF-O',
      Room: '111',
      Color: '#0000FF'
    }
  ];
  public Areas = this.currentRoutes.map(route => route.Area);
  public IDF = this.currentRoutes.map(route => route.Location);
  public Rooms = this.currentRoutes.map(route => route.Room);
  private inputRoute = this.currentRoutes.filter(X => X.Type === 'AudioInput');
  private outputRoute = this.currentRoutes.filter(X => X.Type === 'AudioOutput');

  public cardForArea = false;
  public cardForIDF = false;
  public cardForRoom = false;
  public Input = false;
  public Output = false;
  public Home = true;
  public NewAreas = this.Areas.filter((a, b) => this.Areas.indexOf(a) === b);
  public NewIDF = this.IDF.filter((a, b) => this.IDF.indexOf(a) === b);
  public NewRooms = this.Rooms.filter((a, b) => this.Rooms.indexOf(a) === b);

  public cardInfo: Route[];
  public filterAreaInput: Route[];
  public filterAreaOutput: Route[];
  public sorByRoomInput: Route[];
  public sorByIDFInput: Route[];
  public sorByIDFOutput: Route[];
  public sorByRoomOutput: Route[];


  public VideoInputRoute = this.currentRoutes.filter(X => X.Type === 'VideoInput');
  public VideoOutputRoute = this.currentRoutes.filter(X => X.Type === 'VideoOutput');
  public dataSource = new MatTableDataSource(this.cardInfo);
  public searchData: Route [];
  public InputText: string;

  constructor() {}

  ngOnInit() {
    this.sortCards();
    this.LoadData();
    this.cardForArea = false;
    this.cardForIDF = false;
    this.cardForRoom = false;
    this.Input = false;
    this.Output = false;
    this.Home = true;
  }

  public goHome() {
    this.cardForArea = false;
    this.cardForIDF = false;
    this.cardForRoom = false;
    this.Input = false;
    this.Output = false;
    this.Home = true;
  }

  private sortCards() {
    this.sorByIDFInput = this.inputRoute.sort((X, C) => {
      if (X.Location < C.Location) {
        return 1;
      } else {
        return -1;
      }
    });

    this.sorByRoomInput = this.sorByIDFInput.sort((X, C) => {
      if (X.Room > C.Room) {
        return 1;
      } else {
        return -1;
      }
    });

    this.sorByIDFOutput = this.outputRoute.sort((X, C) => {
      if (X.Location < C.Location) {
        return 1;
      } else {
        return -1;
      }
    });

    this.sorByRoomOutput = this.sorByIDFOutput.sort((X, C) => {
      if (X.Room > C.Room) {
        return 1;
      } else {
        return -1;
      }
    });
  }

  public InPut() {
    this.Input = true;
    this.Output = false;
    this.getCardData();
  }
  public OutPut() {
    this.Output = true;
    this.Input = false;
    this.getCardData();
  }

  public getCardByArea(area: string) {
    this.Home = false;
    this.cardForArea = true;
    this.cardForIDF = false;
    this.cardForRoom = false;
    this.filterAreaInput = this.sorByRoomInput.filter(X => X.Area === area);
    this.filterAreaOutput = this.sorByRoomOutput.filter(X => X.Area === area);
  }
  public getCardByIDF() {
    this.Home = false;
    this.cardForIDF = true;
    this.cardForArea = false;
    this.cardForRoom = false;
  }
  public getCardByRoom() {
    this.Home = false;
    this.cardForIDF = false;
    this.cardForArea = false;
    this.cardForRoom = true;
  }

  public getCardData() {
    if (this.cardForIDF === true && this.Input === true) {
        this.cardInfo = this.sorByIDFInput;
    }
    if (this.cardForIDF === true && this.Output === true) {
       this.cardInfo = this.sorByIDFOutput;
    }
    if (this.cardForRoom === true && this.Input === true) {
      this.cardInfo = this.sorByRoomInput;
    }
    if (this.cardForRoom === true && this.Output === true) {
       this.cardInfo = this.sorByRoomOutput;
    }
    this.dataSource = new MatTableDataSource(this.cardInfo);
  }

  // search maybe
  public applyFilter(InputText) {
    this.getCardData();
    this.dataSource.filter = InputText.trim().toLowerCase();
    this.searchData = this.dataSource.filteredData;
    console.log(this.searchData);
  }
  public LoadData() {
    return this.searchData;
  }

}
